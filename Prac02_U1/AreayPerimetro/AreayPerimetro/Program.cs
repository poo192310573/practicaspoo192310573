﻿using System;

namespace AreayPerimetro
{
    class Program
    {
        static void Main(string[] args)
        {
            //variables
            double radio, area;
            const double PI = 3.1416;
            //Entrada
            System.Console.Write("valor del radio: ");
            radio = double.Parse(Console.ReadLine());
            //Desarrollo
            area = PI * Math.Pow(radio, 2);
            //Datos de salida
            System.Console.Write("area es igual a: " + area);
            System.Console.ReadKey();
            {
                //variables
                double r, perimetro;
                const double General = 3.1416;
                //Entrada
                System.Console.Write("\n\nvalor del radio:");
                r = double.Parse(Console.ReadLine());
                //Deasarrollo
                perimetro = 2 * General * r;
                //Datos de salida
                System.Console.Write("perimetro es igual a:" + perimetro);
                System.Console.ReadKey();
            }

        }
    }
}
