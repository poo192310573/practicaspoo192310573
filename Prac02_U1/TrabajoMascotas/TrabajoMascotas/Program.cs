﻿using System;

namespace TrabajoMascota
{
    class Program
    {
        class Mascota
        {
            private string color;
            private string raza;
            private string tipo;
            private double peso;
            private int anios;

            public void Inicializar()
            {
                Console.WriteLine("Cual es el color de tu mascota?");
                color = Console.ReadLine();

                Console.WriteLine("Cual es la raza de tu mascota?");
                raza = Console.ReadLine();
                tipo = obtenerTipoMascota();
                Console.WriteLine("Cual es el peso de tu mascota?");
                peso = double.Parse(Console.ReadLine());
                Console.WriteLine("Cuantos años tiene tu mascota?");
                anios = int.Parse(Console.ReadLine());
            }

            private string obtenerTipoMascota()
            {
                string resultado = "Sin mascota";

                Console.WriteLine("Selecciona tu tipo de mascota?");
                Console.WriteLine("1 Perro");
                Console.WriteLine("2 Gato");
                Console.WriteLine("3 Serpiente");
                Console.WriteLine("4 Hamster");
                string lineaConsola = Console.ReadLine();
                switch (int.Parse(lineaConsola))
                {

                    case 1:
                        resultado = "Perro";
                        break;

                    case 2:
                        resultado = "Gato";
                        break;

                    case 3:
                        resultado = "Serpiente";
                        break;

                    case 4:
                        resultado = "Hamster";
                        break;

                    default:
                        resultado = "Sin mascota";
                        break;
                }
                return resultado;
            }

            public void vacunarMascota()
            {
                switch (tipo)
                {
                    case "Perro":
                        if (anios > 2)
                        {
                            Console.WriteLine("vacunar a tu mascota");
                        }
                        break;
                    case "Gato":
                        if (anios > 2)
                        {
                            Console.WriteLine("vacunar a tu mascota");
                        }
                        break;
                    case "Serpiente":
                        if (anios > 5)
                        {
                            Console.WriteLine("vacunar a tu mascota");
                        }
                        break;
                                case "Hamster":
                        if (anios > 3)
                        {
                            Console.WriteLine("vacunar a tu mascota");
                        }
                        break;


                }

            }
            public void imprimir()
            {
                Console.WriteLine(color);
                Console.WriteLine(raza);
                Console.WriteLine(tipo);
                Console.WriteLine(peso);
                Console.WriteLine(anios);
            }


            static void Main(string[] args)
            {
                Mascota mascota1 = new Mascota();
                mascota1.Inicializar();
                mascota1.imprimir();
                mascota1.vacunarMascota();

                Console.WriteLine("tienes otra mascota SI/NO");
                string lineaConsola = Console.ReadLine();
                if (lineaConsola == "SI")
                {
                    Mascota mascota2 = new Mascota();
                    mascota2.Inicializar();
                    mascota2.imprimir();
                    mascota2.vacunarMascota();

                }
            }
        }
    }
}