﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class School
{
    
    private List<Student> students;
    public School()
    {
       
        students = new List<Student>();
    }
    
    public void addStudent(Student nuevoEstudiante)
    {
        students.Add(nuevoEstudiante);
    }
    
    public bool buscarPorNombre(String name)
    {
        {
            bool encontrado = false;
            int i = 0;
            while (encontrado == false && i < students.Count)
            {
                if (students[i].name == name)
                {
                    encontrado = true;
                }
                else
                {
                    i++;
                }
            }
            if (encontrado)
            {
                Console.WriteLine("Name: " + students[i].name + "\n"
                    + "Last Name: " + students[i].lastName + "\n"
                    + "Qualification: " + students[i].qualification);
                return false;
            }
            else

            {
                Console.WriteLine("Ese alumno no existe, corrija el nombre");
                return true;
            }


        }
    }
}


public class Student
{
    public String name { get; set; }
    public String lastName { get; set; }
    public int qualification { get; set; }
}


class Program
{
    static void Main(string[] args)
    {
        Student s1 = new Student()
        {
            name = "Gerardo",
            lastName = "Programador",
            qualification = 70
        };
        Student s2 = new Student()
        {
            name = "Tadeo",
            lastName = "Programador",
            qualification = 80
        };
        Student s3 = new Student()
        {
            name = "Norberto",
            lastName = "Programador",
            qualification = 90
        };
        Student s4 = new Student()
        {
            name = "Miguel",
            lastName = "Programador",
            qualification = 70
        };
        School school = new School();
        school.addStudent(s1);
        school.addStudent(s2);
        school.addStudent(s3);
        school.addStudent(s4);
        bool valor = false;
        List<Student> students = new List<Student>();
        do
        {
            Console.WriteLine("Ingresa el nombre del estudiante: ");
            String name = Console.ReadLine();
            valor = school.buscarPorNombre(name);
        } while (valor);
        Console.ReadKey();
    }
}


