﻿using System;

namespace PRACT4_U2_EJ4
{
    class Burbuja 
    {
        private int[] lista; 

        public void LlenadoDeLista() 
        {
            Console.WriteLine("Listas con metodo burbuja");
            Console.Write("¿Cuantos elementos tendra la lista?: ");
            string linea;
            linea = Console.ReadLine(); 
            int cant; 
            cant = int.Parse(linea); 
            lista = new int[cant]; 
            for (int f = 0; f < lista.Length; f++)
            {
                Console.Write("Ingrese elemento " + (f + 1) + ": "); 
                linea = Console.ReadLine(); 
                lista[f] = int.Parse(linea); 
            }
        }
       
        public void MetBurbuja() 
        {
            int t; 
            for (int l = 1; l < lista.Length; l++) 
                for (int m = lista.Length - 1; m >= l; m--) 
                {
                    if (lista[m - 1] > lista[m]) 
                    {
                        t = lista[m - 1]; 
                        lista[m - 1] = lista[m]; 
                        lista[m] = t; 
                    }
                }
        }

        public void Resultado() 
        {
            Console.WriteLine("Lista en forma ascendente");
            for (int f = 0; f < lista.Length; f++)
            {
                Console.Write(lista[f] + "  "); 
            }
            Console.ReadKey(); 
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            
            Burbuja kg = new Burbuja();

            kg.LlenadoDeLista();
            kg.MetBurbuja();
            kg.Resultado();
        }
    }
}


