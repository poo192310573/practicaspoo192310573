﻿using System;

namespace EJ1_Pract1_U2
{
    class Program
    {
        static void Main(string[] args)
        {
            //llamar a una clase
            Operaciones objeto = new Operaciones();
            //llamar metodos
            objeto.LeerMatriz();
            objeto.ImprimirMatriz();
            Console.ReadLine();
        }

    }
    class Operaciones
    {
        public int n;
        public int m;
        public int[,] matriz;
        public void ImprimirMatriz()
        {
            //recorrido
            for (int i = 0; i < n; i++)
            {
                for (int x = 0; x < m; x++)
                {
                    //imprimir matriz
                    Console.WriteLine("El dato de la matriz: " + matriz[i, x]);
                }
            }
        }
        public void LeerMatriz()
        {
            //solicitud de una dimension de la matriz
            Console.WriteLine("Numero de columnas de la matriz: ");
            n = int.Parse(Console.ReadLine());
            //solictud de otra dimension de la matriz
            Console.Write("Numero de filas de la matriz: ");
            m = int.Parse(Console.ReadLine());
            //declaracion de dimensiones de la matriz
            matriz = new int[n, m];
            //recorrido de la matriz
            for (int i = 0; i < n; i++)
            {
                for (int x = 0; x < m; x++)
                {
                    //llenando la matriz
                    Console.WriteLine("Dato de la matriz: ");
                    matriz[i, x] = int.Parse(Console.ReadLine());
                }
            }
        }

    }
}

   
