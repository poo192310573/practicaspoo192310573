﻿using System;

namespace EJ1_Pract1_U2
{
    class Program
    {
        static void Main(string[] args)
        {
            //crear un objeto de clase
            operaciones objeto = new operaciones();
            //llamar todo
            objeto.SumaArreglo();
            //llamar metodo
            objeto.ImprimirPares();
            Console.ReadLine();
        }
    }
    class operaciones
    {
        //Se crea el arreglo
        public int[] n;
        //Tamaño Global
        public int tamaño;
        public void SumaArreglo()
        {
            //Tamaño del arreglo
            Console.WriteLine("el tamaño del arreglo es:");
            //El usuario ingresa el arreglo
            tamaño = int.Parse(Console.ReadLine());
            //Se asigna el tamaño al valor del tamaño del arreglo
            n = new int[tamaño];
            //llenar el arreglo con un valor
            for (int i = 0; i < tamaño; i++)
            {
                Console.WriteLine("El dato del arreglo es:");
                n[i] = int.Parse(Console.ReadLine());
            }

        }
        public void ImprimirPares()
        {
            int pares;
            for (int i = 0; i < tamaño; i++)
            {
                if (n[i] % 2 == 0)
                {
                    Console.WriteLine("numero par:" + n[i]);
                }
            }

        }
    
    }

    
}
